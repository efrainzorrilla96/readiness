---
layout: markdown_page
title: "PostgreSQL Checksums - Design"
---

## On this page
{:.no_toc}

- TOC
{:toc}
## Resources

Epic: [gl-infra/171](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/171)


## Design

This document explains the benefits and functioning of the PostgreSQL data checksums feature. Additionally, it will present our approach to how we will enable them in our production database. Furthermore, we will cover how to execute automated verifications to avoid different types of data corruption.


In essence, a checksum is a calculated number added to all the data page headers from the database.  If you would like to see how the data page header structure looks, check the reference at: https://www.postgresql.org/docs/11/storage-page-layout.html.

The validation of the header of the data page happens when Postgresql reads it, at the moment when it is entering the memory managed by PostgreSQL, the buffer cache. In case of any change of the data in memory, a new checksum is calculated when the data page leaves the PostgreSQL buffer cache.

To better understand how the checksum process works, we need to review how PostgreSQL manages the memory’s data cache process.

### How PostgreSQL manages the cache in memory?

Because of its lightweight nature, PostgreSQL relies on the operating system cache. It depends on the operating system to interact with the file system, disk layout, and reading and writing the data files.

![diagram](diagram.png)

When you execute a statement as “select * from table”, the selected data is loaded first in the OS cache and then loaded to shared buffers, that is the part of the memory that PostgreSQL controls. 
 
When we have some data in the buffer cache and it is updated, we have a dirty buffer in the OS cache to be updated in the disk. To update the data in the disk,  we have a two step process: 
Firstly, it is necessary to flush this data into the disk. 
Secondly, for this data page to be consolidated in the disk,  a separate system call for it is required, and this call is named `fsync()`. 
 
The PostgreSQL `shared_buffers` is partially duplicating the OS memory management, where we will have the same pages in the shared_buffers and the OS cache. 
PostgreSQL validates the page when it comes from the OS cache to the shared buffers and when it leaves the shared_buffer to the OS cache. During these transitions, the data page validation happens and it is where the checksums are being calculated and validated. 

### What are checksums in Postgresql? How does it work?

Since version 9 from PostgreSQL, it is possible to create a database with checksums enabled.
When we enable checksums, we are writing a calculated checksum on the header of each data page. 
 

The checksum calculation is based on an algorithm that uses the FNV-1a hash (FNV is shorthand for Fowler/Noll/Vo).  The original plain FNV-1a hash is resolved in data of 1 byte at a time due to the formula:

	  ` hash = (hash ^ value) * FNV_PRIME`
.
For an in-depth understanding, the FNV-1a algorithm is described at http://www.isthe.com/chongo/tech/comp/fnv/.

PostgreSQL doesn't use FNV-1a hash directly because it has a tendency of high bits. The high bits order in the input data only affects the higher bits order in output data. For performance reasons, we choose to combine 4 bytes at a time. The current hash formula used as the basis is:
 
      `hash = (hash ^ value) * FNV_PRIME ^ ((hash ^ value) >> 17)`
 
To have more details about the implementation, please read the comments of the source code here: https://gitlab.com/postgres/postgres/-/blob/REL_11_STABLE/src/include/storage/checksum_impl.h#L20.


To calculate the checksum, PostgreSQL considers two inputs: 

All the single byte of the data page.
the page offset/address. 

PostgreSQL detects if a byte is changed on the page and detects if a valid page has dropped into the wrong place.
For reading operations, Postgresql calculates and validates the checksum. For updates and inserts, a new checksum value is re-calculated. It is written to the buffer cache into the operating system page cache ( the pages get written to disk later by a separate `fsync` call).


### What happens in case of data corruption:

In the case of data corruption, we will face an error message such as: 
 
`ERROR: invalid page in block 1556710 of relation base/16750/27234`
 
The message above indicates an error with the checksum validation on this page.


PostgreSQL performs two different validation checks on pages. If you find an additional Warning right before the error message such as the following: 
 
`WARNING: page verification failed, calculated checksum 3438 but expected 343152.`
 
We could face the following scenarios :
If you have the above warning message in the log (page verification failed), there is a mismatch between the checksum recorded and the checksum calculated for the data page. This would probably point us to a problem “outside” the database – networking, OS, storage, memory, etc.
If you do not have the above warning in the log (page verification failed), it means the health check failed on the data page header. Problems inside and outside the database could cause this.

Depending on data corruption, the database can shutdown or not. One approach would be executing a pg_checksums check on the whole database to list all the bad checksums. And we could proceed with the investigations executing: 
 - execute a dump from the database or a pg_dumpfile from a table to evaluate the data corruption.
 - Some commands that would help us to verify the state of the data page in the OS:  dd, od, hexdump.

Furthermore, we need to investigate and act depending on case to case:

 - If the corruption happens on one read-only host, we can shut down this host and recreate a new replica.
 If the data corruption is on the primary node, we find that the data page refers to an index. We can disable the index and recreate it.
 - if we have corruption on a data page from a table, we could find the rows referent to this data page and delete them. With this step, we could ensure database integrity. The next step is to restore the data lost from a backup or a delayed replica.
 - Depending on the case, we can execute a vacuum and [reindex](https://www.postgresql.org/docs/11/sql-reindex.html) on the table or database that could solve the problem.
 - In case of corruption from the data dictionary or other cases, we could start the database with developer options as `zero_damaged_pages` or `ignore_system_indexes`, but involves some risks as the [documentation explains](https://www.postgresql.org/docs/current/runtime-config-developer.html).
 - As the documentation mentions, as a last resort, we could execute a (pg_resetwal)[https://www.postgresql.org/docs/11/app-pgresetwal.html] to clears the write-ahead log and optionally resets some other control information stored in the pg_control file from the database. 


### How will we implement this in our GitLab.com PostgreSQL database cluster, without downtime?

To enable the checksums, the database has to be down. The process will read and add a checksum in all the data pages from the database. 

To execute this without generating any downtime, we will take the following steps. The first part will be executing the following steps on the secondaries, node by node :

Stop the traffic to the node.
Drain the traffic to not generate any impact on the application.
Stop Patroni that manages the PostgreSQL process.
Execute the pg_checksums binary to enable the checksums ( this process is single-threaded. We can expect this process to take hours to process 7TB of data).
Execute a verification process in the database with the pg_checksums.
Start Patroni.
Monitor logs.
Restore the traffic to the node.

After, we will execute a switchover from the primary to another node from the cluster.

The last step will be executing the process above again on the old primary, which at this moment is secondary.

### Monitoring and alerting

To ensure that our team is aware of any data corruption error on the database cluster, we will add to our monitoring a filter to the PostgreSQL logs for data corruption errors. When this filter matches, it will trigger an alert. Furthermore, we will have a runbook to support the initial steps of the troubleshooting and possible resolution of the data corruption.

### Other tests to check different types of corruption:

The idea of our project is to enable checksums. Nevertheless, we are also focused on automating the following different CI pipelines to verify the integrity of the database consistently. The concept adopted in these CI pipelines requires restoring a backup (using the [restore project](https://ops.gitlab.net/gitlab-com/gl-infra/gitlab-restore/postgres-gprd)), having a recent copy from the production database.Afterward, we will have the scenario required to execute tests without adding load to the production environment. The checks that will be created are:

#### AM Check

Using the (amcheck)[] module, we will have functions available that will allow us to verify the logical consistency of indexes.  The added value with this check is that we are covering and detecting more types of failures that the (checksums will fail to find.)[] 

#### Dump the database to /dev/null 

The purpose of this test is to execute a read of all the data pages pointing to /dev/null, triggering that the value of the checksum is recalculated and has to be equal to the value of the data page header. Basically, we are scanning the whole database without generating all the I/O required in production.
Eventually, we could simulate a data corruption, to verify that our alerts and checks are working properly.

#### Dump the database and restore

This pipeline will restore a physical backup. Generate a logical backup  ( with pg_dump) and restore it. With this verification, we are covering that there is no corruption at a logical level (such as duplicates violating existing unique constraint).
