# Container Registry Migration Phase 2

## Summary

- [ ] **Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.**
- [ ] **What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?**

### Context

The Package team has been working on the upgrade and migration of the GitLab.com Container Registry ([&5523](https://gitlab.com/groups/gitlab-org/-/epics/5523)) to a new version backed by a new [metadata database (DB)](https://docs.gitlab.com/ee/architecture/blueprints/container_registry_metadata_database) and an [online Garbage Collector (GC)](https://gitlab.com/gitlab-org/container-registry/-/issues/199).

The registry codebase was forked internally into two code paths, old and new, to make it possible to perform a gradual online migration on GitLab.com. All requests are served without the DB and using the old bucket "partition" on the old code path. The new code path makes use of the DB and the new bucket partition. The GCS storage bucket was also "partitioned" into two, one partition for each code path. The old code path kept the default "partition", and the new one stores data under a new `gitlab` prefix on the bucket's root.

This migration approach was detailed, widely discussed, and approved in [gitlab-org/container-registry#374](https://gitlab.com/gitlab-org/container-registry/-/issues/374). It is *highly* recommended to read through this issue description for an overview and additional context. This section will only provide a summary of the most relevant details for this specific readiness review.

The project and the migration plan itself is split into two main parts:

* Phase 1 ([&6426](https://gitlab.com/groups/gitlab-org/-/epics/6426)): This phase is complete and had [its own readiness review](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/container-registry-metadata-db/index.md). During this phase we gradually increased the number of new repositories routed through the new code path and therefore registered on the DB and stored under the new bucket prefix. As of 2022-01-19, any new container repository is automatically created on the new code path, with no exceptions. Please refer to the respective readiness review and the [relevant section](https://gitlab.com/gitlab-org/container-registry/-/issues/374#phase-1-the-metadata-db-serves-new-repositories) of the migration plan for additional context.

* Phase 2 ([&6427](https://gitlab.com/groups/gitlab-org/-/epics/6427)): This is what this readiness review is about. With Phase 1 now complete, we're ready to start Phase 2. We will be importing old container repositories from the old code path to the new one during this phase. This requires scanning the metadata of these repositories under the old bucket prefix and registering it on the metadata DB. This also requires copying image layers from the old bucket prefix into the new one. The rationale behind this approach was widely discussed and thoroughly detailed in the [relevant section](https://gitlab.com/gitlab-org/container-registry/-/issues/374#phase-2-migrate-existing-repositories) of the migration plan.

### Goal

Completing the registry migration Phase 2 will allow us to use the metadata DB and online GC for all container registry repositories (both old and new), which will unlock major cost savings and unblock the implementation of several features, as described in the [original architecture blueprint](https://docs.gitlab.com/ee/architecture/blueprints/container_registry_metadata_database/#challenges).

At the time of writing, there are already over 30k top-level namespaces and 330k container repositories being served by the new registry (or new code path). This currently accounts for close to 20% of all the registry API traffic in production. All these metrics are available in the dedicated [migration dashboard](https://dashboards.gitlab.net/d/registry-migration/registry-migration-detail?orgId=1&from=now-1h&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-migration_path=new). With the progress of Phase 2 we should see the traffic on the old code path trending to zero and all repositories being served exclusively by the new code path and benefiting from its features, cost and performance improvements.

## Architecture

- [ ] **Add architecture diagrams to this issue of feature components and how they interact with existing GitLab components. Include internal dependencies, ports, security policies, etc.**
- [ ] **Describe each component of the new feature and enumerate what it does to support customer use cases.**
- [ ] **For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**
- [ ] **If applicable, explain how this new feature will scale and any potential single points of failure in the design.**

Please refer to the [architecture blueprint](https://docs.gitlab.com/ee/architecture/blueprints/container_registry_metadata_database/#new-architecture) for a detailed description of the service architecture, including availability concerns, failure scenarios, and their impact. Please note that there are zero changes to the architecture in Phase 2. All changes introduced to support this phase are internal only, both on Rails and Container Registry, and all sit behind feature flags disabled by default. There are no new service components, architecture, or deployment changes.

The way that the migration is going to be performed has been detailed in the [relevant section](https://gitlab.com/gitlab-org/container-registry/-/issues/374#phase-2-migrate-existing-repositories) of the migration plan. This plan was built, discussed and reviewed in collaboration with the Delivery and Scalability teams. 

The migration will be driven by GitLab Rails background workers controlled by feature flags that will control the progress of the migration. The full implementation details can be seen [here](https://gitlab.com/groups/gitlab-org/-/epics/7316#note_897867569), including all the application settings and feature flags at our disposal and how the background workers operate. This design was created in collaboration with the Scalability team and all the changes have been reviewed by the [assigned DRIs](https://gitlab.com/groups/gitlab-org/-/epics/5523#dris). In the Operational Risk Assessment section, we talk about the dependencies and what can happen if those fail.

## Operational Risk Assessment

- [ ] **What are the potential scalability or performance issues that may result with this change?**
- [ ] **List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the it will be impacted by a failure of that dependency.**
- [ ] **Were there any features cut or compromises made to make the feature launch?**
- [ ] **List the top three operational risks when this feature goes live.**
- [ ] **What are a few operational concerns that will not be present at launch, but may be a concern later?**
- [ ] **Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**
- [ ] **Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**
- [ ] **As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**

During the migration, each registry instance will be performing extra work, i.e. (pre)importing old repositories into the metadata DB (PostgreSQL) and new bucket prefix (GCS). This extra work will occur while continuing to serve the usual image pull/push, tag delete and tag list requests. (Pre)importing repositories requires additional reads on the old bucket prefix, additional writes on the new bucket prefix, and additional reads and writes on the metadata DB.

It is unfeasible to accurately estimate the additional load generated by the import as that depends on multiple factors, mainly the size of repositories, which we do not know in advance and directly influences the duration of each (pre)import. However, we do have complete control over the pace at which we decide to proceed at any given time using the Rails feature flags and the registry [migration settings](https://gitlab.com/gitlab-org/container-registry/-/blob/eac4f07bd7d3da0cc9afa6d5c2ce9a5fa82520d4/docs/configuration.md#migration) (namely `maxconcurrentimports` and `tagconcurrency`). Paired with the observability we have in place, we are able to start slow and gradually accelerate, remaining within safe boundaries all the time and being able to slow down, or even stop completly if necessary using the main feature flag (`container_registry_migration_phase2_enabled`).

As for the GCS bucket, at the time of writing, we are currently hovering in the range of 1.5k-3k RPS ([source](https://dashboards.gitlab.net/goto/iP1_Oay7k?orgId=1)), from which reads are the vast majority ([source](https://dashboards.gitlab.net/goto/Nva9O-snz?orgId=1)). This sits confortably below the initial _soft_ ceiling of 5k object reads RPS advertised by Google ([source](https://cloud.google.com/storage/quotas)) and should provide enough headroom. We already had solid observability around the bucket usage and have improved it further with a metric that measures `429 Too Many Requests` responses from GCS at the application level ([source](https://dashboards.gitlab.net/d/registry-storage/registry-storage-detail?orgId=1&viewPanel=10)). This provides us with yet another tool to detect when/if we are going too fast and slow down if so. Additionally, we have proactively reached out to the GCP TAM to inform that an RPS increase in the horizon, keeping a communication channel open in case something is needed.

In regards to the metadata DB, in theory there is plently of headroom to accomodate the extra load. At the time of writing, we are hovering around 1.25k RPS, with an average application side connection pool saturation under 10% ([source](https://dashboards.gitlab.net/goto/zArp5asnk?orgId=1)). We are similarly confortable at the PGBouncer level ([source](https://dashboards.gitlab.net/goto/QioA5-y7z?orgId=1)).

Finally, it is important to mention that after completing Phase 1, the new code path is currently already handling 15-20% of all the registry traffic ([source](https://dashboards.gitlab.net/goto/Jn3Nt-ynz?orgId=1)), and this has been steadily increasing. Out of the +2.1M repositories ([source](https://console.postgres.ai/gitlab/gitlab-production-tunnel-pg12/sessions/9488/commands/33652)), only +300k are on the new code path ([source](https://dashboards.gitlab.net/goto/64iGIYs7z?orgId=1)). Therefore, only 1/7 of the repositories are new, but the traffic generated by these represents 1/5. This tells us that a good portion of the old repositories (some 6+ years old) is likely abandoned and therefore we should not see a proportial increase of load on the new code path once we import them over. This is especially true because the overall registry traffic is largely dominated by a not so large number of namespaces ([source](https://log.gprd.gitlab.net/app/lens#/edit/b2768220-efa7-11eb-bfb9-1fcb9ca7f31a?_g=h@c823129)).

On the Rails side, the migration largely depends on background workers supported by Sidekiq and the main database. The execution of the migration is therefore dependent on the availability and reliability of these. In case of a failure, the migration will be stalled (no more repositories will be migrated) until the issue resolved. In terms of performance, we are not certain on how these will operation at scale as we lack a test environment that comes (remotely) close to production in terms of load and data size. However, these were developed following our best practices and reviewed/approved by the Scalability team. 

The main background worker has several limits (namely [job deduplication](https://docs.gitlab.com/ee/development/sidekiq/idempotent_jobs.html#until-executing) and limited [capacity](https://gitlab.com/groups/gitlab-org/-/epics/7316#note_897867569)) in place to avoid clogging the existing jobs queue or flooding workers with jobs. In addition, we are using an [exclusive lease](https://gitlab.com/gitlab-org/gitlab/-/blob/b0f8a7a4c19de201eb22371e07face7c66078a9a/app/services/concerns/exclusive_lease_guard.rb). This avoids having burst conditions where several background jobs will try to contact the Container Registry API at the same time.

Having said that, we expect a _sustained_ load by the main background worker. In the worst case scenario, we could have max capacity (`25`) jobs running concurrently. During testing, we observed that a single job execution could lead to two jobs (re enqueues) running concurrently. This gives us an estimation of `50` jobs running concurrently. This would be the worst sustained load that this background worker adds. As explained above, we have many limits to counter this situation. The main one here is the max capacity that can be set to `1`. In that case, the estimated worst case sustained load drops to `2`.

### Customer Interaction and Possible Impacts

The migration process should be transparent, and the new code path is fully retro-compatible with the functionality provided by the old one. So there should be no visible behavior change. In general, the only visibile change should be a performance increase for read operations that are thus far slow (such as listing tags) on a given repository (once it is fully migrated) as those are much faster when leveraging the metadata DB.

As for possible impacts, as explained in the [migration plan](https://gitlab.com/gitlab-org/container-registry/-/issues/374#phase-2-migrate-existing-repositories), the two-pass import approach requires a small period of read-only mode when performing the final import. During this period, Rails will refuse to serve JWT tokens with write permissions for the registry, as a way to enforce the read-only mode. Some UI interactions such as deleting an image repository or deleting a tag during that time will not be accepted during that time. This is a known and accepted tradeoff to guarantee data consistency. A [blog post](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/101923) was put together and both TAMs and Support are aware of this.

In theory, the probability of a conflicting write request for a given repository happening during the second pass for that same repository should be negligible because:

- The GitLab.com registry write rate is extremely low, more precisely ~5% ([source](https://thanos.gitlab.net/graph?g0.expr=(sum(%0A%20%20%20%20rate(%0A%20%20%20%20%20%20registry_http_requests_total%7B%0A%20%20%20%20%20%20%20%20env%3D%22gprd%22%2C%0A%20%20%20%20%20%20%20%20method%3D~%22put%7Cpatch%7Cpost%7Cdelete%22%2C%0A%20%20%20%20%20%20%20%20code%3D~%22%5E2.*%7C404%22%0A%20%20%20%20%20%20%7D%5B2d%5D%0A%20%20%20%20)%0A)%0A%2F%20%0Asum(%0A%20%20%20%20rate(%0A%20%20%20%20%20%20registry_http_requests_total%7B%0A%20%20%20%20%20%20%20%20env%3D%22gprd%22%2C%0A%20%20%20%20%20%20%20%20method!%3D%22options%22%2C%0A%20%20%20%20%20%20%20%20code%3D~%22%5E2.*%7C307%7C404%22%0A%20%20%20%20%20%20%7D%5B2d%5D%0A%20%20%20%20)%0A))%20*%20100&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D));
- There is a very large number of repositories across which the write rate is distributed;
- Based on a previous production registry inventory and analysis ([source](https://gitlab.com/gitlab-org/container-registry/-/issues/384)), we know that the _vast_ majority of repositories has a _very_ small number of tags/images in them;
- The final import is going to be very fast. In a test on staging, we completed the final import of a repository with 100 tags (which is the default maximum number of tags that a repository can have to be migrated in the first batch in production) in 4 seconds ([source](https://gitlab.com/groups/gitlab-org/-/epics/7528#note_885443545)).

Additionally, Docker, the main registry client, automatically retries any rejected requests during up to 30 seconds before giving up. Considering all of these factors, we believe to be very unlikely that a customer will run into a disruptive write contention situation.

Nevertheless, in case of an unexpected bug (when even the Rails feature to automatically cancel long running imports fails) or emergency, if a subset of migrations gets stuck on the "importing" state in either Rails or the registry, engineers can revert this by force flipping the import status from "importing" to "aborted". Doing so has two effects:

- **Rails:** Stops refusing to serve JWT tokens with write permissions for repositories that were on the "importing" state;
- **Registry:** Inbound API requests for a repository present on the metadata database are only routed through the new code path if the migration status is set to "import complete". By flipping the status to "aborted" we can force the registry to restablish the routing of all requests to the old code path (where the original data will still be present).

To further reduce risk, we are adding a list of VIP customer namespaces (including the registry top users) to a deny list feature flag, ensuring they are excluded from the initial stage of the migration where we are more likely to find unexpected issues. Additionally, we will perform the migration in several stages, starting with `gitlab-org` repositories, then repositories on the free plan, then those on paid plans, and only then the VIP ones.

## Database

- [ ] **If we use a database, is the data structure verified and vetted by the database team?**
- [ ] **Do we have an approximate growth rate of the stored data (for capacity planning)?**
- [ ] **Can we age data and delete data of a certain age?**

The Container Registry DB development adheres to the same [rules and principles](https://docs.gitlab.com/ee/development/database/) defined for the GitLab Rails database. Every change that touches the database requires a review and approval of the Database team.

The database schema, migrations, queries, and partitioning strategy were reviewed and optimized in strict collaboration with the Database team. The main issues where one can find more details about this are [gitlab-org/container-registry#104](https://gitlab.com/gitlab-org/container-registry/-/issues/104) and [gitlab-org/container-registry#228](https://gitlab.com/gitlab-org/container-registry/-/issues/228).

During the development process, we came up with an estimate for what is the expected size of the data once the whole migration is complete, plus a 10x growth projection. All can be found [here](https://gitlab.com/gitlab-org/container-registry/-/merge_requests/566#gitlabcom-production-projections).

There is no concept of aged data on the Container Registry, but the new version includes an [online garbage collection](https://gitlab.com/gitlab-org/container-registry/-/blob/446d2df43c27ecfdcf288b07a58baded275f026d/docs-gitlab/db/online-garbage-collection.md) feature that takes care of deleting untagged artifacts both from the new storage backend prefix and the database.

## Security and Compliance

- [ ] **Were the [gitlab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?**
- [ ] **If this feature requires new infrastructure, will it be updated regularly with OS updates?**
- [ ] **Has effort been made to obscure or elide sensitive customer data in logging?**
- [ ] **Is any potentially sensitive user-provided data persisted? If so is this data encrypted at rest?**
- [ ] **Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.**

No new customer data is exposed in logs, neither new data (beyond what is already present in the storage backend) will be saved on the metadata database. 

The most explicit piece of data stored on the database is the full path of each container repository, which includes the GitLab namespace/group(s) and projects name. The remaining data pertains to container images, namely their manifest (recipe) and runtime configuration payloads. All this data is already managed by the registry but only present on the storage backend. The database is not encrypted.

No new data is exposed at the API level. The import functionality is only acessible internally from Rails and not acessible to users.

There are no new pieces of infrastucture or storage mediums introduced in this phase of the project.
Approval from Legal and Compliance to use the metadata DB for storing the registry data was obtained during Phase 1 in [gitlab-com/legal-and-compliance#632](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/632/).

### InfraSec checklist

* **1. Identify**
  1. [x] Is the Architecture documented?
     1. https://docs.gitlab.com/ee/architecture/blueprints/container_registry_metadata_database/
     1. An [issue has been created](https://gitlab.com/gitlab-org/gitlab/-/issues/358415) to update the [development architecture](https://docs.gitlab.com/ee/development/architecture.html) as well as our [production architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/) (in particular the [DB section](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/#database-architecture))_
  1. [x] Do we have documented processes for the following topics.
     1. [x] Infrastructure changes
        1. _None_. The database was already deployed in [Phase 1](https://gitlab.com/gitlab-org/container-registry/-/issues/374#phase-1-the-metadata-db-serves-new-repositories)
     2. [x] Configuration changes
        1. Delivery team, on the kubernetes side. The new [`chart`](https://gitlab.com/gitlab-org/charts/gitlab/-/tree/master/charts/registry) is applied by Delivery team.
     3. [x] Access requests
        1. There's no access that can be requested for this service. The standards [access requests](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/) procedures are followed for gitlab. 
  1. [x] Do we use IaC for everything?
     1. Yes: kubernetes workloads project ([k8s-workload](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/)), no changes to that
  1. [ ] Secure Software Development Life Cycle (SSDLC). Code Reviews? Static code analysis tools? Image scanners? Where is the terraform state stored and who has access to it?
      1. In the projects identified:
         1. [container-registry](https://gitlab.com/gitlab-org/container-registry): ([#ID-1](#obs-ID-1))
            1. [Vulnerabilities over SLO (180 days)](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/inventory/-/issues/316) 
         1. [CNG](https://gitlab.com/gitlab-org/build/CNG): ([#ID-2](#obs-ID-2))
            1. [Container Scanning is not configured](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/inventory/-/issues/647)
            1. [Dependency Scanning is not configured](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/inventory/-/issues/648)
            1. [SAST is not configured](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/inventory/-/issues/649)
            1. [Secret Detection is not configured](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/inventory/-/issues/650)
         1. [GitLab Charts](https://gitlab.com/gitlab-org/charts/gitlab/): ([#ID-3](#obs-ID-3))
            1. [SAST is not configured](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/inventory/-/issues/37)
      1. No terraform state.

* **2. Protect**
  1. [ ] Configuration according to standards? (AWS Well architected, Center for Internet Security (CIS), Cloud Security Alliance (CSA), and National Institute of Standards and Technology (NIST))
     1. The configuration was done in phase 1 already, no change in phase 2.
  1. [x] Identity and Access Management
     1. [x] How do we handle **Authentication** to access infrastructure (SAML, MFA, etc...)
        1. _Authorised team members can open an issue to [request an access to the metadata DB](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/)._
     1. [x] How do we handle Authentication to access **Authorization** to access infrastructure (IAM Roles, RBAC...) - Should follow the least privilege principle
        1. _It's an issue to [request an access](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/)._
  1. [X] Data protection
     1. [X] What kind of data is stored on each system? (secrets, customer data, audit, etc...)
        1. _Full path of each container repository, which includes the GitLab namespace/group(s) and projects name. The remaining data pertains to container images, namely their manifest (recipe) and runtime configuration payloads. All this data is already managed by the registry but only present on the storage backend._
     1. [X] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html)
        1. Only metadata is stored in the new DB. Full path of each container registry which includes GitLab namespace/group(s) and projects name are classified as [ORANGE](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html#yellow), the remaining is classified [YELLOW](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html#yellow).
     1. [x] Is data it encrypted at rest?
        1. _GCS disks of the metadata DB and the GCS bucket are both encrypted at rest with Google-managed keys_
     1. [x] Do we have backups for this system? How are these protected?
        1. _No backups for the buckets, but we retain [30 days](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/3c714d8d9477aaf04b2c86ece126d82290997632/environments/gprd/main.tf#L2513) of non-current (deleted) objects around. For the Metadata DB, we do have backups that have been verified as working in [gitlab-com/gl-infra/delivery#2056](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2056) and are monitored with [Prometheus metrics](https://thanos.gitlab.net/graph?g0.expr=gitlab_job_success_timestamp_seconds%7Bresource%3D%22walg-basebackup%22%2C%20type%3D%22patroni-v12-registry%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=2d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)._
     1. [x] Process to who can access data and in what conditions?
        1. _GitLab standard process for DB and GCS bucket_
     1. [x] Do we have audit logs on data access?
        1. Yes, for both the GCS bucket ([source](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets/-/blob/95d5617488ed2165cdb99d29395e26f13aa89c5a/bucket_registry.tf#L17)) and the database (`auth.log` and PgBouncer logs). All logs are sent to ElasticSearch, then archived in GCS.
  1. [ ] Network security (for Kubernetes, managed services and VMs)
     1. [X] Do we have documented network architecture and data flows?
        1. _[General Network Architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/#network-architecture)_
        2. _The GitLab architecture documentation will be updated later on in https://gitlab.com/gitlab-org/gitlab/-/issues/358415._
     1. [X] Firewalls follow the least privilege principle
        1. _yes, see [networkpolicy](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/650ad98f8bbe30c061104a7a112081ee0336d4e7/releases/gitlab/values/values.yaml.gotmpl#L356)_
     1. [X] How do we handle encryption in transit? - Internal and external
        1. External http is redirected to https and https is terminated at the haproxy frontend LB, internally we use unencrypted http.
        1. `.Values.global.psql.ssl` is not defined, meaning there's no mutual TLS between the registry and PostgreSQL. The [default configuration is disabled by default](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/charts/gitlab/gprd/charts/registry/values.yaml#L340). Follow-up issue [here](https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1331)
     1. [X] Is the service covered by DDoS protection
        1. _no, registry is not behind CloudFlare._
     1. [ ] Is the service covered by WAF
        1. _no_
  1. [X] Which infrastructure Security controls do we have in place (AWS Service control policies (SCPs), Kubernetes Policies, SELinux, etc...)
     1. _This is not applicable._

* **3 Detect**
  1. [X] Are infrastructure Audit logs are enabled?
     1. _Yes, logs are available in the [GCP console](https://console.cloud.google.com/iam-admin/audit?referrer=search&project=gitlab-production), based on the standard GCP IAM logging feature ([docs](https://cloud.google.com/iam/docs/audit-logging))._
  1. [ ] Are infrastructure Audit logs are being forwarded to our SIEM
     1. No, there are gcp-audit-logs for some GCP projects available in Panther but not for firewall, network or database logs. Follow-up issue [here](https://gitlab.com/gitlab-com/gl-security/security-operations/infrastructure-security/bau/-/issues/148) 
  1. [X] Do we have a Vulnerability detection process?
     1. Yes, see https://about.gitlab.com/handbook/engineering/security/vulnerability_management/
  1. [ ] Do we have an Intrusion Detection Systems deployed (GuardDuty, Falco, osquery, etc...)
     1. _We do not have intrusion detection systems for K8s deployments currently. We do have osquery on our DB nodes, but it currently seems to be inactive in gprd (but working in gstg)._
  1. [X] Do we have alarms on resource usage too high (AWS bill or K8s resources)
     1. _Yes, we have extensive monitoring and alerting on resource saturation._

* **4. Respond**
  1. [X] Containment (Can we block specific users/IPs? Can we lock leaked credentials/tokens?)
     1. _We can block IPs on haproxy. We also are able to lock user accounts and JWT tokens should have a short TTL._
  1. [ ] Forensic collection of evidence process and tools in place
  1. [ ] Vulnerability Patch management process is defined

### Kics scanner results

Scaning locally with kics gave the following results for this [chart registry commit](https://gitlab.com/gitlab-org/charts/gitlab/-/tree/775548c3377ce985d0a49bb8514048c71e46f720/charts/registry) :

```shell

                   .0MO.
                   OMMMx
                   ;NMX;
                    ...           ...              ....
WMMMd     cWMMM0.  KMMMO      ;xKWMMMMNOc.     ,xXMMMMMWXkc.
WMMMd   .0MMMN:    KMMMO    :XMMMMMMMMMMMWl   xMMMMMWMMMMMMl
WMMMd  lWMMMO.     KMMMO   xMMMMKc...'lXMk   ,MMMMx   .;dXx
WMMMd.0MMMX;       KMMMO  cMMMMd        '    'MMMMNl'
WMMMNWMMMMl        KMMMO  0MMMN               oMMMMMMMXkl.
WMMMMMMMMMMo       KMMMO  0MMMX                .ckKWMMMMMM0.
WMMMMWokMMMMk      KMMMO  oMMMMc              .     .:OMMMM0
WMMMK.  dMMMM0.    KMMMO   KMMMMx'    ,kNc   :WOc.    .NMMMX
WMMMd    cWMMMX.   KMMMO    kMMMMMWXNMMMMMd .WMMMMWKO0NMMMMl
WMMMd     ,NMMMN,  KMMMO     'xNMMMMMMMNx,   .l0WMMMMMMMWk,
xkkk:      ,kkkkx  okkkl        ;xKXKx;          ;dOKKkc


Scanning with Keeping Infrastructure as Code Secure 1.5.1


10:36AM INF Scanning with Keeping Infrastructure as Code Secure 1.5.1
10:36AM INF Loading queries of type: kubernetes, ansible
Preparing Scan Assets:  -                                                                                                                                                                                                    10:37AM INF Inspector initialized, number of queries=361
10:37AM INF Query execution timeout=1m0s
Preparing Scan Assets: Done
10:37AM ERR failed to parse file content: /gitlab/charts/registry/templates/configmap.yaml error="failed to parse yaml: invalid yaml"
10:37AM ERR failed to parse file content: /gitlab/charts/registry/templates/deployment.yaml error="failed to parse yaml: invalid yaml"
10:37AM ERR failed to parse file content: /gitlab/charts/registry/templates/hpa.yaml error="failed to parse yaml: invalid yaml"
10:37AM ERR failed to parse file content: /gitlab/charts/registry/templates/ingress.yaml error="failed to parse yaml: invalid yaml"
10:37AM ERR failed to parse file content: /gitlab/charts/registry/templates/migrations-job.yaml error="failed to parse yaml: invalid yaml"
10:37AM ERR failed to parse file content: /gitlab/charts/registry/templates/networkpolicy.yaml error="failed to parse yaml: invalid yaml"
10:37AM ERR failed to parse file content: /gitlab/charts/registry/templates/pdb.yaml error="failed to parse yaml: invalid yaml"
10:37AM ERR failed to parse file content: /gitlab/charts/registry/templates/service.yaml error="failed to parse yaml: invalid yaml"
10:37AM ERR failed to parse file content: /gitlab/charts/registry/templates/serviceaccount.yaml error="failed to parse yaml: invalid yaml"
10:37AM ERR failed to parse file content: /gitlab/charts/registry/templates/servicemonitor.yaml error="failed to parse yaml: invalid yaml"
Executing queries: [---------------------------------------------------] 100.00%

Files scanned: 13
Parsed files: 3
Queries loaded: 361
Queries failed to execute: 0

------------------------------------

Container Running With Low UID, Severity: MEDIUM, Results: 1
Description: Check if containers are running with low UID, which might cause conflicts with the host's user table.
Platform: Kubernetes

        [1]: values.yaml:308

                307: securityContext:
                308:   runAsUser: 1000
                309:   fsGroup: 1000


Passwords And Secrets - Generic Secret, Severity: HIGH, Results: 1
Description: Query to find passwords and secrets in infrastructure code.
Platform: Common

        [1]: values.yaml:245

                244:     #     privatekeySecret:
                245:     #       secret: cdn-private-key
                246:     #       key: private.pem



Results Summary:
HIGH: 1
MEDIUM: 1
LOW: 0
INFO: 0
TOTAL: 2

10:37AM INF Files scanned: 13
10:37AM INF Parsed files: 3
10:37AM INF Queries loaded: 361
10:37AM INF Queries failed to execute: 0
10:37AM INF Inspector stopped
10:37AM INF Results saved to file results.json fileName=results.json
Results saved to file results.json
Generating Reports: Done
Scan duration: 5.927149623s
10:37AM INF Scan duration: 5.927149623s
```

#### Observations

| ID | Observation | Recommendation | Issue | Likeihood | Severity | Risk Rating |
| -- | ----------- | -------------- | ----- | --------- | -------- | ----------- |
| ID-1 <a name="obs-ID-1"/> | [2 critical vulnerabilities](https://gitlab.com/gitlab-org/container-registry/-/security/vulnerability_report) are still waiting for triage. The [triage velocity](https://gitlab-com.gitlab.io/gl-security/engineering-and-research/inventory/vulnerabilities/triage/) of this project increased recently. While these scanners can report false-positives, they also prove to be useful and catch low hanging fruits | Continue efforts and triage at least the Critical findings | https://gitlab.com/gitlab-com/gl-security/engineering-and-research/inventory/-/issues/316 | Possible | Undesirable | 8 | 
| ID-2 <a name="obs-ID-2"/> | Required Security Scanners not configured in CNG: Code not being scanned for potiential security issues, possible secrets leaked, possible supply chain attacks via malicious dependencies | Configure SAST, SAST-IaC, Dependency Scanning, Secret Detection| https://gitlab.com/gitlab-com/gl-security/engineering-and-research/inventory/-/issues/647 https://gitlab.com/gitlab-com/gl-security/engineering-and-research/inventory/-/issues/648 https://gitlab.com/gitlab-com/gl-security/engineering-and-research/inventory/-/issues/649 https://gitlab.com/gitlab-com/gl-security/engineering-and-research/inventory/-/issues/650 | Possible | Undesirable | 8 |
| ID-3 <a name="obs-ID-3"/> | Required Security Scanners not configured in GitLab Charts: Charts not being scanned for potiential security issues, possible secrets leaked, possible supply chain attacks via malicious dependencies | Configure SAST, SAST-IaC | https://gitlab.com/gitlab-com/gl-security/engineering-and-research/inventory/-/issues/37 | Possible | Undesirable | 8 |

## Performance

- [ ] **Explain what validation was done following GitLab's [performance guidlines](https://docs.gitlab.com/ce/development/performance.html) please explain or link to the results below**
    * [Query Performer](https://docs.gitlab.com/ce/development/query_recorder.html)
    * [Sherlock](https://docs.gitlab.com/ce/development/profiling.html#sherlock)
    * [Request Profiling](https://docs.gitlab.com/ce/administration/monitoring/performance/request_profiling.html)
- [ ] **Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?**
- [ ] **Are there any throttling limits imposed by this feature? If so how are they managed?**
- [ ] **If there are throttling limits, what is the customer experience of hitting a limit?**
- [ ] **For all dependencies external and internal to the application, are there retry and back-off strategies for them?**
- [ ] **Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**

The development of the registry and Rails logic took into account the GitLab performance guidelines. The performance of all introduced database queries (in both sides) was tested and reviewed following the described process and approved by database maintainers. The behavior design and implementation of Rails background workers was done in close collaboration with the Scalability team. 

As explained in the Operational Risk Assessment section, there will be increased load on the registry metadata DB and GCS bucket, but we do not expect these to become problematic, and if they do, we have a way to stop the migration to mitigate any side effects. We should be able to accomodate a steady load increase from the migration workload at the same time that we continue to process regular API traffic and handling brief spikes.

There are no throttling limits at the registry level for the introduced migration feature. This feature is only available internally (called from GitLab Rails), no external interaction is allowed and therefore we have a high degree of control over it. On the Rails side, background workers run at predefined cadences and/or in response to relevent events. There are multiple application settings and feature flags that allow us to control the overall migration throughtput (and therefore the load on the registry as well).

## Backup and Restore

- [ ] **Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?**
- [ ] **Are backups monitored?**
- [ ] **Was a restore from backup tested?**

The database is subject to backups, including a delayed replica for situations where we may have to rollback in time to investigate and fix a data issue.

The storage backend needs no additional backups. We have [object versioning](https://cloud.google.com/storage/docs/object-versioning) turned on for the production GCS bucket. This is currently configured to keep up to [30 days](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/3c714d8d9477aaf04b2c86ece126d82290997632/environments/gprd/main.tf#L2513) of non-current (deleted) objects around, which means we can recover any acidentally deleted data from the storage backend if necessary as well.

The database backup restore tests have been verified as working in [gitlab-com/gl-infra/delivery#2056](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2056).

[Backups](https://thanos.gitlab.net/graph?g0.expr=gitlab_job_success_timestamp_seconds%7Bresource%3D%22walg-basebackup%22%2C%20type%3D%22patroni-v12-registry%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=2d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
and
[backup verifications](https://thanos.gitlab.net/graph?g0.expr=gitlab_job_success_timestamp_seconds%7Bresource%3D%22https%3A%2F%2Fops.gitlab.net%2Fgitlab-com%2Fgl-infra%2Fgitlab-restore%2Fpostgres-gprd%20restore-verify%22%2Ctype%3D%22postgres-registry%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=2d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
are monitored based on emitted Prometheus metrics.

## Monitoring and Alerts

- [ ] **Is the service logging in JSON format and are logs forwarded to logstash?**
- [ ] **Is the service reporting metrics to Prometheus?**
- [ ] **How is the end-to-end customer experience measured?**
- [ ] **Do we have a target SLA in place for this service?**
- [ ] **Do we know what the indicators (SLI) are that map to the target SLA?**
- [ ] **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**
- [ ] **Do we have troubleshooting runbooks linked to these alerts?**
- [ ] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**
- [ ] **do the oncall rotations responsible for this service have access to this service?**

### Registry

Registry logs are JSON formatted and available in ELK. A log entry is produced for every (pre)import. Detailed log entries can be filtered by using a correlation ID. Each API request to initiate a (pre)import carries a unique correlation ID and that ID is propagated downstream to all individual log entries.

Additional Prometheus metrics were implemented and added to the Grafana dashboards:

- [Migration Detail](https://dashboards.gitlab.net/d/registry-migration/registry-migration-detail?orgId=1&from=now-6h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&var-migration_path=new):

  - Count of top-level namespaces and repositories registered on the metadata DB;
  - Traffic share for the new code path;
  - RPS, latency and error rate of import API requests;
  - Rate, latency and number of inflight imports (broken down by import type);
  - Rate and latency of failed imports (broken down by import type);
  - Worker saturation per instance.

- [Storage Detail](https://dashboards.gitlab.net/d/registry-storage/registry-storage-detail):

  - Rate of `429 Too Many Requests` responses from GCS (used to monitor possible rate limits imposed by Google).

Apart from these, we have access to a broad set of metrics related to the database in the [Database Detail](https://dashboards.gitlab.net/d/registry-database/registry-database-detail) dashboard.

There are no changes to the end-to-end customer experience. The API latency and error rate remain as the best indicator of these. The service SLA and corresponding SLIs also remains unchanged, and so do alerts.

### Rails

Rails logs are also JSON formatted and available in ELK. The Rails workers produce logs for every execution and can be easily identified.

Additional Prometheus metrics were implemented and added to the [Migration Detail](https://dashboards.gitlab.net/d/registry-migration/registry-migration-detail?orgId=1&from=now-6h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&var-migration_path=new) Grafana dashboards:

   - Count of repositories pending of an import, imported, (pre)importing, skipped or aborted;
   - Overall completion percentage.

As for new alerts, and taking into account the possibility of stalled imports, we are creating specific row count metrics and alerts to track these in [gitlab-org/gitlab#357515](https://gitlab.com/gitlab-org/gitlab/-/issues/357515).

Rails background jobs activity is monitored through a [Kibana dashboard](https://log.gprd.gitlab.net/goto/11194a00-b40c-11ec-b73f-692cc1ae8214).

## Responsibility

- [ ] **Which individuals are the subject matter experts and know the most about this feature?**
- [ ] **Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**
- [ ] **Is someone from the team who built the feature on call for the launch? If not, why not?**

The primary subject matter experts are [@jdrpereira](https://gitlab.com/jdrpereira) (registry) and [@10io](https://gitlab.com/10io) (Rails) from the Package team. [@hswimelar](https://gitlab.com/hswimelar) (registry), [@jaime](https://gitlab.com/jaime) (registry) and [@sabrams](https://gitlab.com/sabrams) (Rails) are also heavily involved in the development of this project. Stable counterparts from Database, Delivery, Distribution and Scalability teams are listed [here](https://gitlab.com/groups/gitlab-org/-/epics/5523#dris).

The Package team will take responsibility for the reliability of the feature once it is in production. 

@jdrpereira is available to be on call for the launch if necessary.

## Testing

- [ ] **Describe the load test plan used for this feature. What breaking points were validated?**
- [ ] **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**
- [ ] **Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**

The end-to-end migration procedure was throughly tested on Staging in [gitlab-org&7528](https://gitlab.com/groups/gitlab-org/-/epics/7528). The table in the description provides a list of all test cases, including provoking/simulating failures.

In regards to load tests, and as for Phase 1, we ackowledge that no environment comes close to production in terms of load and data diversity. Therefore, it unfeasible to simulate a realistic load. Fortunately, we have a way to start slow and gradually increase the migration throughput, as well as pausing/resuming whenever we find the need to.

As for tests that run automatically in GitLab's CI/CD pipeline for this feature, the Container Registry codebase includes a large number of integration tests that run as part of the repository CI pipeline. These provide full coverage for the migration API. As for Rails background workers, these cannot be tested as part of QA tests, so their behaviour was throughly validated on staging.

Apart from the Container Registry repository CI pipelines, QA tests validate the existing functionality and integration with GitLab Rails and the ability to push/pull images with Docker.
