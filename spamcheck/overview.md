# Anti-Spam Engine (spamcheck) Readiness Review

## Summary

The Anti-spam engine is a service that GitLab.com will communicate with when a new Issue is created, to determine whether it is spam. If it is determined to be spam, Gitlab.com will, in the future, block the creation of that Issue.

**This service is being prepared [in the GitLab Security Department GCP hierarchy in our security-automation live instance of a GKE/Kubernetes cluster for go-live](https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/16#note_532885404). It will eventually move to a more standardized environment as part of a future readiness review but this review is for Security Automation architecture with use by GitLab.com.**

[Currently GitLab.com does not block issues because reCAPTCHA is enabled](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/app/services/spam/spam_verdict_service.rb) and therefore both Akismet and Spamcheck return `CONDITIONAL_ALLOW` as the most restrictive result.

The round-trip time is for issue creation. It should not be disproportionately affected and we will track seconds and compare against a baseline.

[Once it's determined how verdicts received by .com from spamcheck will be tracked and logged](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/68), we should monitor that the amount of data each spamcheck call generates corresponds to the expected and we log it accordingly.

Since, at first, we will operate spamcheck in so-called monitoring mode, the metrics above make sense for that scenario. Later, however, we’ll operate spamcheck in enforcing mode, where .com will indeed enact verdicts provided by spamcheck. The metrics at that point will likely change considerably (FP, TP, FN, TN, etc.)

**Candidate metrics for the future**

* Number of Issues that are manually reported as spam by users
* Number of spam Issues that have to be handled by the T&S team (number cleaned up by Bouncer)
* Number of legitimate Issues that are blocked from creation
* Number of public Issues created or updated  in GitLab.com
* Number of Issues handled by the service (likely same as Akismet )
* Number of Issues indicated as spam by Akismet vs as indicated by our service

Also, [see metrics related to Akismet](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/68#note_527174794), from it we see that we can expect around 200k-400k calls per month.


## Architecture

![Anti-Spam Engine Architecture](./architecture.png)

Please refer to https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/12818#details and https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck for an up-to-date portrayal of spamcheck's architecture.

We are using Knative to provide auto-scaling as needed. Spamcheck is currently running in a regional GKE cluster maintained by the SecAuto team, a regional outage would make the service unavailable.

In https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/60 we are implementing an allowlist for projects so we can slowly ramp up the number of projects we provide verdicts for.

## Operational Risk Assessment

Issue creation may take longer than is currently normal. Consequently, this could result in poor UX experience for the customers due to the waiting times (if spamcheck becomes unresponsive and every call to it times out).

Spamcheck calls the [Inspector](https://gitlab.com/gitlab-com/security-tools/inspector) service, which is running in the same GKE cluster, for each spam decision. A non-functional Inspector deployment would cause Spamcheck calls to fail open (`ALLOW` verdict), while slow Inspector responses would delay Spamcheck responses in turn.

It only supports Issue creation checking instead of all actually spammable objects (Snippets, Profiles, Comments, Notes) and other objects which are potential vectors such as Profiles, Comments, Notes, MRs, etc.

A UI or API for quick, live configuration changes might also be useful in the future. We will for now rely on deployments to alter live configuration of the service.

**Operational risks for go-live**

1. Issue creation takes unreasonably long
2. Issue creation fails for a large percentage of users
3. The data provided by the initial deployment of spamcheck is not useful in the spirit of https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/68

**Operational concerns not present at launch (may be a concern later)**

1. Throughput of spammable objects to check and the impact on the latency of their creation
2. Deploying with no downtime
3. Updating the service configuration

**Disabling the service**

Yes - the external spam URL in the configuration UI can be removed which will keep Gitlab.com from trying to contact the service

**Customer interaction**

The user will indirectly interact with the service when they create an issue. If the service fails on this interaction, GitLab.com will use the spam verdict received from Akismet

**Worst-case failure mitigation**

Calling Akismet and Spamcheck (ideally in parallel) with aggressive timeouts and using whatever responses are available on timeout.

## Security and Compliance

All [gitlab security development guidelines](https://about.gitlab.com/security/#gitlab-development-guidelines) were followed for this feature.

**Automatic updates of the environment**

Auto-updates are enabled for our GKE nodes in our Security Automation Kubernetes cluster.

**Customer data encryption and storage**
All customer data is sent to the service encrypted using GRPC TLS and remains encrypted throughout the workflow in Kubernetes using mTLS with Istio. No data is stored by the service.

**Regulatory and compliance standards**
The service is not subject to any regulatory/compliance standards.

## Performance

We are using Knative for auto-scaling to account for traffic spikes. We are going live with a project filter that will allow us to control the amount of requests we process based on an allowlist/denylist configured in the Spamcheck service. Note that the allowlist and denylist cannot both be populated at the same time.
Additionally, a monitoring mode can be enabled that allows us to inspect the verdict results without them affecting the ultimate verdict in GitLab.

## Monitoring and Alerts

The service is using several GCP Monitoring services, which are described in the [Spamcheck Runbook](https://gitlab.com/gitlab-com/gl-security/runbooks/-/blob/master/automation/spamcheck.md).

**Measurements of the end-to-end customer experience**
1. Comprehensive Grafana dashboards?
2. Time to connect to the service
3. Duration of the request
4. Successful issue creation
5. Result code for requests (200, 500 etc)

## Responsibility

**Subject matter experts**

* Alexander Dietrich
* Ethan Urie
* Jayson Salazar
* Juliet Wanjohi

**Responsible team**

Security Automation

**On-call support**

The Security Automation team will be on-call for this feature until the next readiness review, when the infrastructre moves to Infra.

## Testing

**Load test plan**

1. We tested with a simulated 100-1000 requests per second (10,000 requests total) using `ghz`. This is well above the ~6 requests per _minute_ GitLab makes to Akismet. The result was the service managed the load easily and did not need to spin up new instances.

**Test suites**

1. Standard unit tests (_test.go files)
2. SAST (Bandit, Gosec, Brakeman)
3. Secret detection
4. Dependency scanning (Bundler Audit, Gemansium)
5. License scanning

**Unit tests to check**

1. Whether an issue payload contains the title and description fields
2. How an issue is handled based on spam content i.e. non-spam, likely, highly likely and spam issues
3. How an issue is handled if posting to Inspector fails
4. How an issue is handled based on the project allowlist/denylist filter
