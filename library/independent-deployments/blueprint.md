## Independent deployments

### Background

Over the past 4 years the Delivery team has focused on transforming GitLab’s deployment and release process to be safe, efficient, and repeatable. As part of the work to adopt Continuous Delivery we migrated GitLab.com to Kubernetes and built custom release tooling to provide the required flexibility and speed for deployments and rollbacks.

Today, any merged change has a predictable path to reach both GitLab.com and self-managed users.

But our journey doesn’t end here. GitLab has reached the size where not every team has the same deployment needs and not every service is being deployed or released in the same way. Our deployment and release approach must now mature to go beyond our one-size-fits-all approach and offer a level of service that is appropriate to the needs of individual teams.

This blueprint outlines our initial thinking about the design of Independent deployments to enable Stage groups to deploy their own code changes safely into Production.

### The current deployment process

Today, most changes to GitLab.com follow a hands-off, monolithic deployment approach called "Managed auto-deploy"; developers either merge changes for Rails, or prepare version bump MRs for component `*_VERSION` files. In both cases, Release Managers from the Delivery group coordinate the timing and steps of the deployment, either through [managed auto-deploys](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2572#terminology), or by reviewing and merging the version bump MR.

The following deployment steps are applicable to all components for GitLab, including the rails monolith:
![development-to-deployment-overview](/uploads/6f135f544566ae29ffb7b90cc5568860/development-to-deployment-overview.png)
- [Diagram source](https://docs.google.com/presentation/d/1YRjA1dYCXNXp06VltDYlik1MdFyzUvaeXKk69mMPcA4/edit#slide=id.g176981daeea_0_0)
1. Changeset tagging: Developers select the changeset intended to be released and tag it with a new version. The exact details of this step vary depending on component setup but may involve a Chatops tag command or other automation to create a tag for the changeset 
1. Artifact creation: Developers complete the component-specific steps needed to create an artifact containing the tagged changeset
1. Packaging: The application and its dependencies are bundled into container images and Linux packages in preparation for rollout to GitLab.com or self-managed instances
1. Deployment tracking: For change management and compliance needs all deployments are tracked to record the versions included in the package and log the release manager, deployment start times, and overall system health
1. Package validation: Each new package is rolled out to a suitable test environment(s) for testing. Package health is assessed through a combination of automated QA tests and automated system health checks
1. Promotion: Following successful validation, a package is promoted to the GitLab.com production environment 
1. Publishing: Changes that have successfully operated on the GitLab.com environment are available for inclusion in a self-managed release  

### Downsides to the current deployment process

A centralized deployment approach gives maximum control and consistency but denies Stage groups full ownership of their changes and creates a Delivery scaling bottleneck as the number of Stage groups grows.

Some of the challenges with the current deployment process:
1. Running a centralized deployment approach means each deployment contains a large number of changes. This contributes to longer deployment times, delaying some changes from reaching users.
1. Deployment timing and ownership sits with Delivery group. With the current process, Stage groups have limited visibility into the steps and timing of deployments, and are reliant on Release Managers to execute all rollout steps. This prevents Stage groups from being able to own and control the rollout of their code changes to users.
1. Rollbacks provide us with a way to quickly restore Production back to a healthy state. However, rolling back a deployment that contains large numbers of changes affects more changes than the one having trouble. In addition to this, rollbacks are entirely managed by the Delivery team. This prevents Stage groups from understanding the impact of changes and increases the chances of similar problems happening in the future.

### Introducing Independent Deployments

Moving away from centralized deployments to GitLab.com and allowing Stage groups to independently deploy their service will put teams back in charge of the end-to-end development process and deployment cadence. 

Stage groups who own their end-to-end development process can optimize for more stable, and then faster deployments by optimizing code and reducing failures and rollbacks. 

To achieve this without impacting users, Delivery will collaborate with Stage groups to mature the current deployment tools and processes into a deployment system that can be safely operated by any Stage group. For Delivery, this will mark a shift away from building tools for ourselves, and move towards implementing new tools and processes, as well as training and improved documentation to support external users.

For the early iterations we'll focus on providing a self-serve option for deploying code changes for suitable GitLab components.  We will not initially provide a mechanism for configuration or infrastructure changes but these items will be reviewed in the future.

Long-term we’re looking to adjust ownership to move today’s Release Managers into Platform engineers, [Priyanka persona](https://about.gitlab.com/handbook/product/personas/#priyanka-platform-engineer), and allow stage groups to fully own their release management, [Rachel persona](https://about.gitlab.com/handbook/product/personas/#rachel-release-manager).

### Independent deployments

At a high-level, Independent deployments will allow a Stage group to safely deploy a change through to Production. The pipeline, testing, and deployment tracking will all exist specifically for this Stage group, separate from the existing auto-deploy process and tooling.

Like all deployments and releases, the Independent deployment pipelines will include the following functionality: 
1. A dependable pipeline to apply changes in a repeatable manner and following the same changelock restrictions as managed auto-deploys 
1. A deployment record to accurately track what was deployed, when, and by whom
1. Observability to make deployment failures visible and actionable
1. Recoverable. In most cases, we would want to rollback changes when a problem is detected

For GitLab.com we require that all changes pass through our Staging and Production canary and main-stage environments to allow for the detection of problems before they reach users. Only changes that have successfully run on GitLab.com can be released to self-managed users.

To support the testing of GitLab's official image-building process before releasing it to users we expect all changes to support CNG and Omnibus [component requirements](https://gitlab.com/gitlab-org/release/docs/-/blob/master/components/index.md#requirements-for-a-new-component). 

#### Developer experience

To support different team needs, Stage groups will be able to choose between three different deployment strategies:

1. **release train:** deployments follows a fixed timetable, similar to managed auto-deploys. When it's time, a deployment starts on the latest green commit in the main branch.
1. **continuous delivery:** each change merged into the main branch will be deployed to the canary stages, and automatic tests will be executed. A manual promotion is needed to continue the deployment to the main stages.
1. **continuous deployment:** same as continuous delivery but with automatic promotion.

The deployment pipeline will include the required deployment tracking, appropriate testing, and all jobs needed to deploy to the Staging and Production canary and main stage environments.

If the pipeline fails any tests or environment health checks, the change will automatically roll back and inform the developer. In case of continuous delivery and continuous deployment, the pipeline will also revert the offending commit from the main branch to prevent failures on the following packages.

Deployment progress will be visible to the Stage group to allow for full ownership of rollout.  

#### Release Manager experience

Managed auto-deploys will continue to deploy changes for projects that haven't enabled Independent deployments. Using the existing auto-deploy process, Release Managers will deploy changes to the canary and main-stage environments. [Mean Time To Production (MTTP)](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#mean-time-to-production-mttp) will remain as the overall Performance Indicator for deployments. 

Alongside auto-deploys, Independent deployment pipelines will execute. Release Managers will oversee environment usage and deployment tool operation but shouldn't need to be hands-on with any Independent deployment pipeline. If there are failures or incidents that appear to be related to an Independent deployment the Release Manager will be able to rollback to a previous stable state.  

Packaged releases are made up of all changes successfully deployed to GitLab.com. The method of deployment shouldn't change this. In other words a change deployed to GitLab.com through an independent pipeline will be automatically combined into the next packaged release in the same way as a change deployed using the auto-deploy pipeline.

#### Enabling Independent deployments

Stage groups who can meet the documented project requirements for Independent deploys will have the option to enable this deployment approach. Requirements will as a minimum include: 
1. Support for [Managed versioning](https://gitlab.com/gitlab-org/release/docs/-/blob/master/components/managed-versioning/index.md)
2. Rollback support 
3. Appropriate tests and health checks to allow for safe rollout
 
Initially, project enablement will occur in collaboration with the Delivery group to create a new, Independent deployment pipeline to deploy the code to the required environments through to GitLab.com. As part of the pipeline setup, deployment pipeline metrics and [DORA metrics](https://docs.gitlab.com/ee/user/analytics/index.html#devops-research-and-assessment-dora-key-metrics) will be configured to provide observability.

### Deployment optimization

Mean Time To Production ([MTTP](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#mean-time-to-production-mttp)) is the Delivery Group's overall performance indicator (PI) to measure deployment health. With the introduction of Independent deployments, additional metrics will be introduced to track and allow measurable improvements to all GitLab deployments and releases.

Stage groups who own their end to end development process can optimize for more stable, and then faster deployments by optimizing code, and reducing failures and rollbacks. DORA metrics track the current process health and could be used to identify and track improvements.

Delivery group remain responsible for providing a stable deployment system to all Stage groups. Metrics and observability will be a key part of managing environment usage, tool stability, and overall deployment efficiency.
